package uk.co.expressivesoftware.gorkana;


public enum InstructionOperator {

    ADD, SUBTRACT, DIVIDE, MULTIPLY, APPLY;
}
