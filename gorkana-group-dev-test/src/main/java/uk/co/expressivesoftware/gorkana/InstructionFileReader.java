package uk.co.expressivesoftware.gorkana;


import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Lists.transform;
import static uk.co.expressivesoftware.gorkana.InstructionOperator.APPLY;

public class InstructionFileReader {


    public static List<InstructionSet> readLines(String fileLocation) throws IOException {
        File file = FileUtils.getFile(fileLocation);
        List<String> lines = FileUtils.readLines(file);
        lines.removeAll(Arrays.asList("", null));
        List<Instruction> allInstructionLines = transform(lines, new InstructionTransformer());

        return transformToInstructionSets(allInstructionLines);
    }

    private static List<InstructionSet> transformToInstructionSets(List<Instruction> allInstructionLines) {
        List<InstructionSet> instructionSets = newArrayList();

        InstructionSet instructionSet = new InstructionSet();
        List<Instruction> instructions = newArrayList();

        for(Instruction instruction : allInstructionLines){
            if(APPLY.equals(instruction.getInstructionOperator())){
                instructionSet.setStartingValue(instruction.getNumericValue());
                instructionSet.setInstructions(instructions);
                instructionSets.add(instructionSet);
                instructionSet = new InstructionSet();
                instructions = newArrayList();
            }else{
                instructions.add(instruction);
            }
        }

        return instructionSets;
    }


}
