package uk.co.expressivesoftware.gorkana;

import java.util.List;

public class InstructionProcessor {

    public static void main(String[] args) throws Exception {
        String fileLocation = args[0];

        List<InstructionSet> instructionSets = InstructionFileReader.readLines(fileLocation);

        for(InstructionSet instructionSet : instructionSets){
            int result = Calculator.calculate(instructionSet);
            System.out.println(result);
        }

    }
}
