package uk.co.expressivesoftware.gorkana;


import java.util.List;

public class InstructionSet {

    private List<Instruction> instructions;
    private int startingValue;

    public List<Instruction> getInstructions() {
        return instructions;
    }

    public InstructionSet setInstructions(List<Instruction> instructions) {
        this.instructions = instructions;
        return this;
    }

    public int getStartingValue() {
        return startingValue;
    }

    public InstructionSet setStartingValue(int startingValue) {
        this.startingValue = startingValue;
        return this;
    }
}
