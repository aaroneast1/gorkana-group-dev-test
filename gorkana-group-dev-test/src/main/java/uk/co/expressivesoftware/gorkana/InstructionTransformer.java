package uk.co.expressivesoftware.gorkana;


import com.google.common.base.Function;

public class InstructionTransformer implements Function<String, Instruction> {

    @Override
    public Instruction apply(String input) {
        String[] split = input.split(" ");
        String command = split[0];
        String numericalValue = split[1];
        InstructionOperator instructionOperator = InstructionOperator.valueOf(command.toUpperCase());
        Integer numericValue = Integer.valueOf(numericalValue);

        return new Instruction()
                .setInstructionOperator(instructionOperator)
                .setNumericValue(numericValue);
    }

}
