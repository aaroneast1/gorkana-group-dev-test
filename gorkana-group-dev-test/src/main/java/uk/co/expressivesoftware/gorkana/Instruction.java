package uk.co.expressivesoftware.gorkana;

public class Instruction {

    private InstructionOperator instructionOperator;
    private int numericValue;

    public InstructionOperator getInstructionOperator() {
        return instructionOperator;
    }

    public Instruction setInstructionOperator(InstructionOperator instructionOperator) {
        this.instructionOperator = instructionOperator;
        return this;
    }

    public int getNumericValue() {
        return numericValue;
    }

    public Instruction setNumericValue(int numericValue) {
        this.numericValue = numericValue;
        return this;
    }

    @Override
    public String toString() {
        return "Instruction{" +
                "instructionOperator=" + instructionOperator +
                ", numericValue=" + numericValue +
                '}';
    }
}
