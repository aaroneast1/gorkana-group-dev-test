package uk.co.expressivesoftware.gorkana;

public class Calculator {

    public static int calculate(InstructionSet instructionSet) {
        int result = instructionSet.getStartingValue();

        for (Instruction instruction : instructionSet.getInstructions()) {
            result = applyInstruction(result, instruction);
        }

        return result;
    }

    private static int applyInstruction(int currentValue, Instruction instruction) {
        int result = 0;
        switch (instruction.getInstructionOperator()) {
            case ADD:
                return currentValue + instruction.getNumericValue();
            case SUBTRACT:
                result = currentValue - instruction.getNumericValue();
                break;
            case DIVIDE:
                result = currentValue / instruction.getNumericValue();
                break;
            case MULTIPLY:
                result = currentValue * instruction.getNumericValue();
                break;
            default:
                new IllegalArgumentException("Unable to process command");
        }
        return result;
    }


}
