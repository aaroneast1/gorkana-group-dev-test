package uk.co.expressivesoftware.gorkana;


import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.net.URL;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class InstructionFileReaderTest {

    String file;

    @Before
    public void setUp(){
        URL resource = InstructionFileReaderTest.class.getResource("test_instruction_file.txt");
        file = resource.getFile();
    }

    @Test(expected = FileNotFoundException.class)
    public void should_fail_when_invalid_file_given() throws Exception {
        String invalid_file_location = "invalid_file_location";
        List<InstructionSet> lines = InstructionFileReader.readLines(invalid_file_location);
    }

    @Test
    public void should_return_an_empty_list_when_file_contains_no_commands() throws Exception {
        URL resource = InstructionFileReaderTest.class.getResource("empty_command_file.txt");
        String emptyFile = resource.getFile();
        List<InstructionSet> lines = InstructionFileReader.readLines(emptyFile);
        assertThat(lines.size(),is(0));
    }

    @Test
    public void should_read_file() throws Exception {
        List<InstructionSet> instructionSets = InstructionFileReader.readLines(file);
        assertThat(instructionSets.size(), is(1));

        InstructionSet instructionSetOne = instructionSets.get(0);
        List<Instruction> instructions = instructionSetOne.getInstructions();
        assertThat(instructions.size(),is(3));

        Instruction instructionOne = instructions.get(0);
        assertThat(instructionOne.getInstructionOperator(), is(InstructionOperator.ADD));
        assertThat(instructionOne.getNumericValue(), is(2));

        Instruction instructionTwo = instructions.get(1);
        assertThat(instructionTwo.getInstructionOperator(), is(InstructionOperator.MULTIPLY));
        assertThat(instructionTwo.getNumericValue(), is(4));

        Instruction instructionThree = instructions.get(2);
        assertThat(instructionThree.getInstructionOperator(), is(InstructionOperator.DIVIDE));
        assertThat(instructionThree.getNumericValue(), is(8));
    }

    //TODO - Gather requirements for invalid commands.

}
