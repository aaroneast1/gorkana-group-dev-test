package uk.co.expressivesoftware.gorkana;

import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class CalculatorTest {

    Calculator underTest;


    @Before
    public void setUp(){
        underTest = new Calculator();
    }

    @Test
     public void should_calculate_instructionset_with_add_instruction(){
        Instruction instructionLineOne = new Instruction().setInstructionOperator(InstructionOperator.ADD).setNumericValue(5);
        List<Instruction> instructionList = Lists.newArrayList();
        instructionList.add(instructionLineOne);
        InstructionSet instructionSet = new InstructionSet();
        instructionSet.setStartingValue(2);
        instructionSet.setInstructions(instructionList);

        int result = underTest.calculate(instructionSet);

        assertThat(result, is(7));
    }

    @Test
    public void should_calculate_instructionset_with_add_substract_multiply_and_divide_instructions(){
        Instruction instructionLineOne = new Instruction().setInstructionOperator(InstructionOperator.ADD).setNumericValue(5);
        Instruction instructionLineTwo = new Instruction().setInstructionOperator(InstructionOperator.SUBTRACT).setNumericValue(10);
        Instruction instructionLineThree = new Instruction().setInstructionOperator(InstructionOperator.MULTIPLY).setNumericValue(2);
        Instruction instructionLineFour = new Instruction().setInstructionOperator(InstructionOperator.DIVIDE).setNumericValue(3);
        List<Instruction> instructionList = Lists.newArrayList();
        instructionList.add(instructionLineOne);
        instructionList.add(instructionLineTwo);
        instructionList.add(instructionLineThree);
        instructionList.add(instructionLineFour);
        InstructionSet instructionSet = new InstructionSet();
        instructionSet.setStartingValue(20);
        instructionSet.setInstructions(instructionList);

        int result = underTest.calculate(instructionSet);

        assertThat(result, is(10));
    }


}
